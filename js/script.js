function intro() {
	var introContainer = elmsByClass("intro")[0];
	if (!localStorage.getItem("intro")) {
		addClass(introContainer, "show");
		var closeButton = elmsByClass("close")[0];
		addEvent(closeButton, "click", function() {
			hideIntro(introContainer);
			localStorage.setItem("intro", "hide");
		});
	}
}

function hideIntro(introContainer) {
	var title = elmsByClass('site-name')[0];
	removeClass(title, 'site-name');
	elmsByClass('title')[0].appendChild(title);
	removeClass(introContainer, "show");
}


DOMReady(function() {
	Zoom.init();
	intro();
});

