Zoom = {
	init: function() {
		Zoom.body = document.getElementsByTagName("body")[0];
		Zoom.values = {
			"scale-small": "small",
			"scale-normal": "normal",
			"scale-big": "big" 
		}
		Zoom.renderDial();
		if (localStorage.getItem("fontSize")) {
			var fontSize = localStorage.getItem("fontSize");
			addClass(Zoom.body, fontSize);
		}
	},
	scale: function() {
		var size = Zoom.values[this.id];
		for (var i in Zoom.values) {
			if (Zoom.values[i] == size) {
				addClass(Zoom.body, size);
				addClass(elm(i), "active");
			} else {
				removeClass(Zoom.body, Zoom.values[i]);
				removeClass(elm(i), "active");
			}
		}
		localStorage.setItem("fontSize", size);
	},
	renderDial: function() {
		var scaleBlock = elm("scale");
		scaleBlock.innerHTML = "<ul><li id=\"scale-small\"><a href=\"javascript:void(0);\">A</a></li><li id=\"scale-normal\"><a href=\"javascript:void(0);\">A</a></li><li id=\"scale-big\"><a href=\"javascript:void(0);\">A</a></li></ul>";
		addClass(scaleBlock, "scale");
		for (var i in Zoom.values) {
			addEvent(elm(i), "click", Zoom.scale);
			addClass(elm(i), i);
		}
	}
}

